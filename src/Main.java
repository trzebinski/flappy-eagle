import javax.swing.*;

public class Main {
    static int WIDTH = 500;
    static int HEIGHT = 500;

    public static void main(String[] args) {
        JFrame frame = new JFrame("Flappy Eagle");
        frame.setVisible(true);
        frame.setSize(WIDTH, HEIGHT);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Keyboard keyboard = Keyboard.getINSTANCE();
        frame.addKeyListener(keyboard);

        GamePanel gamePanel = new GamePanel();
        frame.add(gamePanel);
    }
}
