import java.awt.*;
import java.awt.geom.AffineTransform;

class BackgroundRender {
    int positionX;
    int positionY;
    AffineTransform affineTransform;
    Image image;

    BackgroundRender() {
    }

    BackgroundRender(int positionX, int positionY, String imagePath) {
        this.positionX = positionX;
        this.positionY = positionY;
        this.image = LoadingImage.loadingImage(imagePath);
    }
}
