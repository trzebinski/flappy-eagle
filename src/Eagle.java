import java.awt.*;
        import java.awt.event.KeyEvent;
        import java.awt.geom.AffineTransform;

public class Eagle {
    private Image image;
    private Keyboard keyboard;
    private int jump;
    private double gravity;
    private double rotation;
    private double yvel;
    boolean isDead;
    int eagleHeight;
    int eagleWidth;
    int eaglePositionX;
    int eaglePositionY;

    public Eagle() {
        keyboard = Keyboard.getINSTANCE();
        gravity = 0.5;
        jump = 0;
        rotation = 0.0;
        yvel = 0;
        isDead = false;
        eagleHeight = 40;
        eagleWidth = 40;
        eaglePositionX = 100;
        eaglePositionY = 225;
    }

    BackgroundRender renderEagle() {
        BackgroundRender backgroundRender = new BackgroundRender();
        backgroundRender.positionX = eaglePositionX;
        backgroundRender.positionY = eaglePositionY;

        if (image == null) {
            image = LoadingImage.loadingImage("libraries/eagle.png");
        }
        backgroundRender.image = image;

        rotation = (90 * (yvel + 20) / 20) - 90;
        rotation = rotation * Math.PI / 180;

        if (rotation > Math.PI / 2) {
            rotation = Math.PI / 2;
        }

        backgroundRender.affineTransform = new AffineTransform();
        backgroundRender.affineTransform.translate(eaglePositionX + (eagleWidth >> 1),
                eaglePositionY + (eagleHeight >> 1));
        backgroundRender.affineTransform.rotate(rotation);
        backgroundRender.affineTransform.translate(-eagleWidth >> 1, -eagleHeight >> 1);

        return backgroundRender;
    }

    void refreshEagle() {
        yvel += gravity;

        if (jump > 0) {
            jump--;
        }

        if (!isDead && keyboard.isDown(KeyEvent.VK_SPACE) && jump <= 0) {
            yvel = -10;
            jump = 10;
        }

        eaglePositionY += (int) yvel;
    }
}
