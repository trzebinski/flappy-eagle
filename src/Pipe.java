import java.awt.*;

class Pipe {
    private Image image;
    String orientation;
    int pipeHeight;
    int pipeWidth;
    int positionX;
    int positionY;

    Pipe(String orientation) {
        this.orientation = orientation;
        resetPipe();
    }

    BackgroundRender renderPipe() {
        BackgroundRender backgroundRender = new BackgroundRender();
        backgroundRender.positionX = positionX;
        backgroundRender.positionY = positionY;

        if (image == null) {
            image = LoadingImage.loadingImage("libraries/pipeTo" + orientation + ".png");
        }
        backgroundRender.image = image;

        return backgroundRender;
    }

    boolean isCollision(int x, int y, int height, int width) {
        int margin = 2;

        if (x + width - margin > positionX && x + margin < positionX + pipeWidth) {
            if (orientation.equalsIgnoreCase("Down") && y < positionY + pipeHeight) {
                return true;
            } else {
                return orientation.equalsIgnoreCase("Top") && y + height > positionY;
            }
        }
        return false;
    }

    void resetPipe() {
        pipeWidth = 66;
        pipeHeight = 400;
        positionX = Main.WIDTH + 2;

        if (orientation.equalsIgnoreCase("Down")) {
            positionY = -(int) (Math.random() * 120) - pipeHeight / 2;
        }
    }

    void refreshPipe() {
        int speed = 3;
        positionX -= speed;
    }
}
