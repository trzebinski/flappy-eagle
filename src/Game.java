import java.awt.event.KeyEvent;
import java.util.ArrayList;

class Game {
    private static final int PIPE = 100;
    private Eagle eagle;
    private Keyboard keyboard;
    private ArrayList<Pipe> pipeList;
    private int restartDelay;
    private int pipeDelay;
    int score;
    Boolean isGameOver;
    Boolean isStarted;

    Game() {
        keyboard = Keyboard.getINSTANCE();
        reset();
    }

    private void startGame() {
        if (!isStarted && keyboard.isDown(KeyEvent.VK_SPACE)) {
            isStarted = true;
        }
    }

    private void restartGame() {
        if (restartDelay > 0)
            restartDelay--;

        if (keyboard.isDown(KeyEvent.VK_R) && restartDelay <= 0) {
            reset();
            restartDelay = 10;
        }
    }

    private void reset() {
        eagle = new Eagle();
        pipeList = new ArrayList<>();
        restartDelay = 0;
        pipeDelay = 0;
        score = 0;
        isStarted = false;
        isGameOver = false;
    }

    private void generatePipes() {
        pipeDelay--;

        if (pipeDelay < 0) {
            pipeDelay = PIPE;
            Pipe pipeToTop = null;
            Pipe pipeToDown = null;

            for (Pipe pipe : pipeList) {
                if (pipe.positionX - pipe.pipeWidth < 0) {
                    if (pipeToTop == null) {
                        pipeToTop = pipe;
                    } else {
                        pipeToDown = pipe;
                        break;
                    }
                }
            }

            if (pipeToTop == null) {
                Pipe pipe = new Pipe("Top");
                pipeList.add(pipe);
                pipeToTop = pipe;
            } else {
                pipeToTop.resetPipe();
            }

            if (pipeToDown == null) {
                Pipe pipe = new Pipe("Down");
                pipeList.add(pipe);
                pipeToDown = pipe;
            } else {
                pipeToDown.resetPipe();
            }

            pipeToTop.positionY = pipeToDown.positionY + pipeToDown.pipeHeight + 175;
        }

        for (Pipe pipe : pipeList) {
            pipe.refreshPipe();
        }
    }

    private void checkCollision() {
        for (Pipe pipe : pipeList) {
            if (pipe.isCollision(eagle.eaglePositionX, eagle.eaglePositionY, eagle.eagleHeight, eagle.eagleWidth)) {
                eagle.isDead = true;
                isGameOver = true;
            } else if (pipe.positionX == eagle.eaglePositionX
                    && pipe.orientation.equalsIgnoreCase("Down")) {
                score++;
            }
        }

        if (eagle.eaglePositionY + eagle.eagleHeight < Main.HEIGHT - 460
                || eagle.eaglePositionY + eagle.eagleHeight > Main.HEIGHT) {
            isGameOver = true;
        }
    }

    ArrayList<BackgroundRender> loadingRenders() {
        ArrayList<BackgroundRender> backgroundRender = new ArrayList<>();
        backgroundRender.add(new BackgroundRender(0, 0, "libraries/background.png"));
        backgroundRender.add(eagle.renderEagle());
        for (Pipe pipe : pipeList) {
            backgroundRender.add(pipe.renderPipe());
        }

        return backgroundRender;
    }

    void mainGame() {
        startGame();

        if (!isStarted)
            return;

        restartGame();

        eagle.refreshEagle();

        if (isGameOver)
            return;

        generatePipes();
        checkCollision();
    }
}
