import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

class LoadingImage {
    private static HashMap<String, Image> MEMORY = new HashMap<>();

    static Image loadingImage(String path) {
        Image image = null;

        if (MEMORY.containsKey(path)) {
            return MEMORY.get(path);
        }

        try {
            image = ImageIO.read(new File(path));

            if (!MEMORY.containsKey(path)) {
                MEMORY.put(path, image);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }
}
