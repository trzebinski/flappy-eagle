import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {
    private static Keyboard INSTANCE;
    private boolean[] keys;

    private Keyboard() {
        keys = new boolean[256];
    }

    public void keyPressed(KeyEvent event) {
        if (event.getKeyCode() >= 0 && event.getKeyCode() < keys.length) {
            keys[event.getKeyCode()] = true;
        }
    }

    public void keyReleased(KeyEvent event) {
        if (event.getKeyCode() >= 0 && event.getKeyCode() < keys.length) {
            keys[event.getKeyCode()] = false;
        }
    }

    public void keyTyped(KeyEvent event) {
    }

    static Keyboard getINSTANCE() {
        if (INSTANCE == null) {
            INSTANCE = new Keyboard();
        }
        return INSTANCE;
    }

    boolean isDown(int key) {
        if (key >= 0 && key < keys.length) {
            return keys[key];
        }
        return false;
    }
}
