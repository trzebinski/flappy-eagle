import javax.swing.*;
import java.awt.*;

public class GamePanel extends JPanel implements Runnable {
    private Game game;

    GamePanel() {
        game = new Game();
        new Thread(this).start();
    }

    protected void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);

        Graphics2D graphics2D = (Graphics2D) graphics;
        for (BackgroundRender backgroundRender : game.loadingRenders()) {
            if (backgroundRender.affineTransform != null) {
                graphics2D.drawImage(backgroundRender.image, backgroundRender.affineTransform, null);
            } else {
                graphics.drawImage(backgroundRender.image, backgroundRender.positionX, backgroundRender.positionY,
                        null);
            }
        }

        graphics2D.setColor(Color.BLACK);

        if (!game.isStarted) {
            graphics2D.setFont(new Font("Arial", Font.PLAIN, 20));
            graphics2D.drawString("Press SPACE to start game", 125, 200);
        } else {
            graphics2D.setFont(new Font("Arial", Font.PLAIN, 30));
            graphics2D.drawString(Integer.toString(game.score), 235, 35);
        }

        if (game.isGameOver) {
            graphics2D.setFont(new Font("Arial", Font.PLAIN, 20));
            graphics2D.drawString("Press R to restart game", 130, 200);
        }
    }

    private void refreshGamePanel() {
        game.mainGame();
        repaint();
    }

    public void run() {
        try {
            while (true) {
                refreshGamePanel();
                Thread.sleep(25);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
