# Flappy Eagle

Project is based on widely available mobile game - Flappy bird.

Technologies
---
Project is created with: Java, Swing

Getting Started
---
In home directory, run the following command to download project:

    git clone https://trzebinski@bitbucket.org/trzebinski/flappy-eagle.git

In downloaded directory, run the following command:

    java -jar FlappyEagle.jar

Screenshot example
---
![SCREENSHOT](https://i.ibb.co/pKY1sB4/SCREENSHOT.png)
